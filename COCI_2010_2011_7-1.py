'''
If N is a multiple of 5, it‟s obviously best to carry only 5-kilogram packages.
If this is not the case, we will have to use at least one 3-kilogram package.

From this we derive the following algorithm: use 3-kilogram packages as long as
remaining number is not a multiple of 5, and then use only 5-kilogram packages to
use up the remaining sugar.

There are other approaches to solving this task. We could use two loops to try out
all the possible combinations of 3 and 5-kilogram packages, and find out which one
gives as the correct amount of sugar in least number of packages used.
'''

N = int(input())

result = -1
fiveBag = N//5
threeBag = 0

while fiveBag >= 0:
    n = N
    if n >= 5:
        n = n-(5*fiveBag)
    if n == 0:
        result = fiveBag+threeBag
        break
    else:
        threeBag = n//3
        n3 = n-(3*threeBag)
        if n3 == 0:
            result = fiveBag+threeBag
            break
    fiveBag -= 1

print(result)